FROM tiangolo/uwsgi-nginx-flask:python3.6
COPY requirements.txt /app
RUN pip3 install -r requirements.txt

ENV UWSGI_INI /app/uwsgi.ini
ENV STATIC_PATH /app/templates/static
ENV STATIC_URL /static
ENV NGINX_WORKER_PROCESSES auto

COPY ./app /app
WORKDIR /app

