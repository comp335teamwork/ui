from base64 import b64decode
from functools import wraps

from flask import Blueprint, render_template, request, jsonify, abort, make_response, redirect, json
import requests

from environment_variables import USER_SERVICE

user_app = Blueprint('user', __name__)


@user_app.route('/account', methods=['GET'])
def account_page():
    session = init_session()
    return render_template('account.html', session=session)


@user_app.route('/account/create-account', methods=['GET'])
def create_account():
    # load the form

    session = init_session()
    session['statuses'] = dict()
    session['statuses']['name'] = ''
    session['statuses']['email'] = ''
    session['statuses']['password'] = ''
    session['statuses']['address'] = ''
    session['statuses']['payment'] = ''

    return render_template('create-account.html', session=session)


@user_app.route('/account/create-account', methods=['POST'])
def account_form_validator():
    # return result of input
    name = request.values.get('name')
    email = request.values.get('email')
    password = request.values.get('password')
    address = request.values.get('address')
    payment = request.values.get('payment')
    type = request.values.get('type')
    data = {
        "name": name,
        "email": email,
        "password": password,
        "address": address,
        "payment": payment,
        "type": type
    }

    session = init_session()
    session['statuses'] = dict()
    session['statuses']['name'] = 'error' if name is '' else ''
    session['statuses']['email'] = 'error' if email is '' else ''
    session['statuses']['password'] = 'error' if password is '' else ''
    session['statuses']['address'] = 'error' if address is '' else ''
    session['statuses'][
        'payment'] = 'error' if payment == '[Select Payment]' else '[Select Payment]'
    session['create-account-endpoint'] = '{}/create-account'.format(USER_SERVICE)

    response = requests.post(session['create-account-endpoint'], json=data).json()
    user_token = response['userToken']

    status = dict()
    status['status'] = response['status']
    session['status'] = status['status']
    return render_template('create-account.html', session=session)


@user_app.route('/account/login', methods=['GET'])
def login():
    # load the form

    session = init_session()
    session['statuses'] = dict()
    session['statuses']['email'] = ''
    session['statuses']['password'] = ''

    return render_template('login.html', session=session)


@user_app.route('/account/logout')
def logout():
    resp = make_response(redirect('/'))
    resp.set_cookie('session-token', '', expires=0)
    return resp


@user_app.route('/account/login', methods=['POST'])
def login_form_validator():
    # post the input and check if credentials are correct

    email = request.values.get('email')
    password = request.values.get('password')
    data = {
        "email": email,
        "password": password
    }

    session = init_session()
    session['statuses'] = dict()
    session['statuses']['email'] = 'error' if email is '' else ''
    session['statuses']['password'] = 'error' if password is '' else ''
    session['login-endpoint'] = '{}/login'.format(USER_SERVICE)

    response = requests.post(session['login-endpoint'], json=data).json()
    status = dict()
    status['status'] = response['status']
    session['status'] = status['status']
    resp = make_response(redirect('/'))
    resp.set_cookie('session-token', response['session'])

    return resp


def requires_roles(*roles):
    def wrapper(f):
        @wraps(f)
        def wrapped(*args, **kwargs):
            if get_current_user_role() not in roles:
                return abort(403)
            return f(*args, **kwargs)

        return wrapped

    return wrapper


def get_user_token():
    session_token = request.cookies.get('session-token')
    data = {
        'sessionToken': session_token
    }
    response = requests.post('{}/session-user'.format(USER_SERVICE), json=data)
    return response.text[12:] if response.status_code is 200 else None


def is_logged_in():
    session_token = request.cookies.get('session-token')
    if session_token is None:
        return False
    return True if get_user_token() is not None else False


def is_admin(user_token=None):
    if user_token is None:
        user_token = get_user_token()
    data = {
        'user-token': user_token
    }

    response = requests.post('{}/is-admin'.format(USER_SERVICE), json=data).json()
    if response.get('isAdmin'):
        return True
    else:
        return False


def get_current_user_role():
    session_token = request.cookies.get('session-token')
    if session_token is None:
        return 'unauthenticated'

    user_token = get_user_token()

    if 'not found' in user_token:
        return 'unauthenticated'

    if is_admin(user_token) is True:
        return 'admin'
    else:
        return 'user'


def init_session():
    session = dict()
    session['logged_in'] = is_logged_in()
    session['admin'] = is_admin()
    if request.cookies.get('meals') is None:
        session['cart'] = None
    else:
        session['cart'] = json.loads(b64decode(request.cookies.get('meals')))
    session['cart_size'] = len(session['cart']) if session['cart'] is not None else 0
    return session
