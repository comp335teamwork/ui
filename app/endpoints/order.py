from decimal import Decimal

from flask import Blueprint, render_template, redirect, make_response
import requests

from endpoints.user import requires_roles, get_user_token, init_session
from environment_variables import ORDER_SERVICE, RESTAURANT_SERVICE, INVENTORY_SERVICE

order_app = Blueprint('order', __name__)


@order_app.route('/orders', methods=['GET'])
@requires_roles('user', 'admin')
def list_orders_for_user():
    user_token = get_user_token()
    session = init_session()

    data = {
        'user-token': user_token
    }

    response = requests.post('{}/list-all-for-user'.format(ORDER_SERVICE), json=data).json()

    meals = []
    meal = dict()
    orders = []

    for response_order in response['orders']:
        meals = []
        for response_meal in response_order['menu-item-tokens']:
            meal = dict()
            meal['name'] = response_meal['name']
            menu_item_cost = Decimal(response_meal['cost'])
            meal['cost'] = f'{menu_item_cost:.2f}'.zfill(2)
            meals.append(meal)
        data = {
            'restaurant-token': response_order['restaurant-token']
        }
        response = requests.post('{}/get-restaurant'.format(RESTAURANT_SERVICE), json=data).json()
        order = dict()
        order['restaurant'] = response['restaurant']['name']
        order['order-token'] = response_order['order-token'].replace('-', '')[5:18]
        order['meals'] = meals
        order['state'] = response_order['state']
        total_order_cost = response_order['cost']
        order['total_cost'] = f'{total_order_cost:.2f}'.zfill(2)
        orders.append(order)

    session['orders'] = orders
    return render_template('orders.html', session=session)


@order_app.route('/make-order', methods=['GET'])
def make_order():
    session = init_session()
    session['cart'] = cart_to_list(session['cart'])
    data = dict()
    menu_items = list()
    user_token = get_user_token()
    data['user-token'] = user_token if 'not found' not in user_token else "anonymous"

    menu_token_restaurant = "restaurant-ed3583c9-041a-4647-b273-9fc4bafe6cf0"
    data['restaurant-token'] = menu_token_restaurant
    for order in session['cart']:
        menu_item = {
            "token": "meal-c352-46e7-9af1-a8704aca5018",
            "name": "Sandwich",
            "cost": "6.76"
        }

        menu_items.append(menu_item)
    data['menu-items'] = menu_items
    response = requests.post('{}/create-order'.format(ORDER_SERVICE), json=data)

    resp = make_response(redirect('/'))
    resp.set_cookie('meals', '', expires=0)
    return resp

@order_app.route('/checkout', methods=['GET'])
def checkout():
    session = init_session()
    session['cart'] = cart_to_list(session['cart'])
    return render_template('checkout.html', session=session)


def cart_to_list(cart):
    items = list()
    for cart_item in cart:
        data = {
            "mealToken": cart_item['meal-token']
        }
        response = requests.post('{}/menu-list/get-meal/meal-token'.format(INVENTORY_SERVICE),
                                 json=data).json()
        name = response['mealName']
        cost = response['cost']
        item = {
            "token": cart_item,
            "name": name,
            "cost": cost
        }
        items.append(item)
    return items
