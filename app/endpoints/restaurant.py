from flask import Blueprint, render_template, json, request, jsonify
import requests

from endpoints.user import requires_roles, init_session
from environment_variables import RESTAURANT_SERVICE, USER_SERVICE, INVENTORY_SERVICE

restaurant_app = Blueprint('restaurant', __name__)


@restaurant_app.route('/create-restaurant', methods=['GET'])
@requires_roles('admin')
def create_restaurant():
    session = init_session()

    response = requests.get('{}/all'.format(USER_SERVICE)).json()
    session['users'] = list()
    for user_response in response:
        user = dict()
        user['name'] = user_response['name']
        user['email'] = user_response['email']
        user['user-token'] = user_response['userToken']
        session['users'].append(user)

    return render_template('create-restaurant.html', session=session)


@restaurant_app.route('/create-restaurant', methods=['POST'])
@requires_roles('admin')
def create_restaurant_post():
    name = request.form.get('restaurant-name')
    description = request.form.get('restaurant-description')
    employees_response = request.form.get('employees').split(',')
    employees = list()
    for employee_response in employees_response:
        employee = {
            'user-token': employee_response
        }
        employees.append(employee)
    data = {
        "restaurant-name": name,
        "restaurant-description": description,
        "employees": employees
    }

    response = requests.post('{}/create-restaurant'.format(RESTAURANT_SERVICE), json=data).json()
    restaurant_token = response['restaurant-token']
    data = {
        "menuName": "Menu",
        "restaurant": name,
        "mealList": []
    }
    response = requests.post('{}/create-menu'.format(INVENTORY_SERVICE), json=data).json()
    data = {
        'restaurant-token': restaurant_token,
        'menu-token': response['menuToken']['menuToken']
    }
    response = requests.post('{}/bind-menu'.format(RESTAURANT_SERVICE), json=data)

    result = dict()
    result['status'] = 'Success'
    return jsonify(result)


@restaurant_app.route('/', methods=['GET'])
def list_restaurants():
    session = init_session()

    response = requests.get('{}/list-all'.format(RESTAURANT_SERVICE))
    data = json.loads(response.text)
    restaurants = list()
    for element in data['restaurant']:
        restaurant = dict()
        restaurant['menu-token'] = element['menu-token']
        restaurant['restaurant-token'] = element['restaurant-token']
        restaurant['description'] = element['description']
        restaurant['name'] = element['name']
        restaurants.append(restaurant)
    session['restaurants'] = restaurants

    return render_template('index.html', session=session)
