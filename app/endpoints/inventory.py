from flask import Blueprint, render_template, request, redirect
import requests

from endpoints.user import requires_roles, init_session
from environment_variables import INVENTORY_SERVICE, RESTAURANT_SERVICE

inventory_app = Blueprint('inventory', __name__)


@inventory_app.route("/restaurant/<restaurant_token>", methods=['GET'])
def get_meal_from_menu(restaurant_token):
    session = init_session()

    data = {
        "restaurant-token": restaurant_token
    }

    session['restaurant-token'] = restaurant_token

    response = requests.post('{}/get-restaurant'.format(RESTAURANT_SERVICE), json=data).json()
    menu = response['restaurant']['menu-token']
    data = {
        "menuToken": menu
    }

    response = requests.post('{}/menu-list/get-menu/menu-token'.format(INVENTORY_SERVICE),
                             json=data).json()
    session['meals'] = response['mealList']

    return render_template('inventory.html', session=session)


@inventory_app.route("/restaurant/<restaurant_token>/menu-list/modify", methods=['GET'])
@requires_roles('admin')
def add_to_menu(restaurant_token):

    session = init_session()

    data = {
        "restaurant-token": restaurant_token
    }

    response = requests.post('{}/get-restaurant'.format(RESTAURANT_SERVICE), json=data).json()
    menu = response['restaurant']['menu-token']

    data = {
        "menuToken": menu
    }
    response = requests.post('{}/menu-list/get-menu/menu-token'.format(INVENTORY_SERVICE),
                             json=data).json()

    session['meals'] = response['mealList']
    session['restaurant-token'] = restaurant_token

    return render_template('add-meal.html', session=session)


@inventory_app.route("/restaurant/<restaurant_token>/menu-list/add", methods=['GET'])
@requires_roles('admin')
def add_to_menu_post(restaurant_token):

    meal_name = request.values.get('mealName')
    cost = float(request.values.get('cost'))
    description = request.values.get('description')
    availability = True if "true" == request.values.get('availability') else False

    temp = {
        "restaurant-token": restaurant_token
    }

    session = init_session()

    response = requests.post('{}/get-restaurant'.format(RESTAURANT_SERVICE), json=temp).json()
    menu = response['restaurant']['menu-token']
    restaurant_name = response['restaurant']['name']

    temp = {
        "menuToken": menu
    }

    response = requests.post('{}/menu-list/get-menu/menu-token'.format(INVENTORY_SERVICE),
                             json=temp).json()
    temp = {

        "menuToken": menu,
        "mealList": [
            {
                "mealName": meal_name,
                "cost": cost,
                "description": description,
                "availability": availability
            }
        ]
    }

    session['meals'] = response['mealList']

    response = requests.post('{}/menu-list/add-meal'.format(INVENTORY_SERVICE), json=temp)

    return redirect('/restaurant/{}/menu-list/modify'.format(restaurant_token))


@inventory_app.route("/restaurant/<restaurant_token>/menu-list/delete", methods=['GET'])
def delete_from_menu_post(restaurant_token):
    session = dict()
    session['logged_in'] = True
    session['admin'] = True

    data = {
        "restaurant-token": restaurant_token
    }

    session['restaurant-token'] = restaurant_token

    response = requests.post('{}/get-restaurant'.format(RESTAURANT_SERVICE), json=data).json()
    menu = response['restaurant']['menu-token']
    data = {
        "menuToken": menu
    }

    response = requests.post('{}/menu-list/get-menu/menu-token'.format(INVENTORY_SERVICE),
                             json=data).json()

    session['meals'] = response['mealList']

    meal_token = request.values.get('mealToken')

    data = {
        "mealToken": meal_token
    }

    response = requests.post('{}/menu-list/delete-meal/meal-token'.format(INVENTORY_SERVICE), json=data)

    return redirect('/restaurant/{}/menu-list/modify'.format(restaurant_token))



