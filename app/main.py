import os

from flask import Flask, send_from_directory
from endpoints.inventory import inventory_app
from endpoints.order import order_app
from endpoints.restaurant import restaurant_app
from endpoints.user import user_app

app = Flask(__name__)
app.register_blueprint(order_app)
app.register_blueprint(user_app)
app.register_blueprint(inventory_app)
app.register_blueprint(restaurant_app)


@app.route('/post', methods=['POST'])
def post_method():
    return 'failure'


@app.route('/static/js/<path:filename>')
def serve_static_js(filename):
    root_dir = os.path.dirname(os.getcwd())
    return send_from_directory(os.path.join(root_dir, 'app', 'templates', 'static', 'js'), filename)


@app.route('/static/css/<path:filename>')
def serve_static_css(filename):
    root_dir = os.path.dirname(os.getcwd())
    return send_from_directory(os.path.join(root_dir, 'app', 'templates', 'static', 'css'),
                               filename)


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True, port=8081)
