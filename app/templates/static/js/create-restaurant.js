$(window).on("load", function () {
    $('#search-select').dropdown();
});


$(function () {
    $(".submit").on("submit", function (event) {
            var isValid = true;
            if (!$.trim($("#name").val())) {
                $("#name").parent("div").addClass("error");
                isValid = false;
            } else {
                $("#name").parent("div").removeClass("error")
            }

            if (!$.trim($("#description").val())) {
                $("#description").parent("div").addClass("error");
                isValid = false;
            } else {
                $("#description").parent("div").removeClass("error")
            }

            if (!$.trim($("#search-select").val())) {
                $("#search-select").parent("div").addClass("error");
                isValid = false;
            } else {
                $("#search-select").parent("div").removeClass("error");
            }
            if (isValid) {
                $.post(
                    "/create-restaurant",
                    {
                        "restaurant-name": $.trim($("#name").val()),
                        "restaurant-description": $.trim($("#description").val()),
                        "employees": $.trim($("#search-select").val()),
                    }
                    ,
                    function (data) {
                        window.location.href = "/";
                    });
            }
            event.preventDefault();
        }
    );
});
