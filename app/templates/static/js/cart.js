$(function () {
    $(".submit").on("submit", function (event) {
            var meals = Cookies.get('meals');
            if (meals == null) {
                meals = []
            } else {
                meals = JSON.parse(atob(meals));
            }

            meals.push({
                "meal-token": this[0].value
            });
            $("#cart").value = meals.length;

            Cookies.set('meals', btoa(JSON.stringify(meals)));

            event.preventDefault();
            return false;
        }
    );
});
