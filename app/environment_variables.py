import os


def noisy_get_env(env_name):
    env_value = os.environ.get(env_name)
    if env_value is None:
        raise ValueError('No environment variable "{}" configured'.format(env_name))
    return env_value


ORDER_SERVICE = os.environ.get('ORDER_SERVICE', 'http://order.mtra-restaurant-application.com')
RESTAURANT_SERVICE = os.environ.get('RESTAURANT_SERVICE',
                                    'http://restaurant.mtra-restaurant-application.com')
INVENTORY_SERVICE = os.environ.get('INVENTORY_SERVICE',
                                   'http://inventory.mtra-restaurant-application.com')
USER_SERVICE = os.environ.get('USER_SERVICE', 'http://user.mtra-restaurant-application.com')
